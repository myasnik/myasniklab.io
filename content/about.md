+++
title = "About me"
date = "2020-09-12"
author = "Myasnik"
cover = "img/face.jpg"
description = "About me"
+++

Hi, my name is Pietro; I was born in January 17, 1998 🎂.

## Education

I attended in my home town, Cesena (Emilia-Romagna):
- Liceo Scientifico Augusto Righi
- Ingegneria e Scienze Informatiche, University of Bologna

In Rome:
- Cybersecurity master course at University of Rome, La Sapienza

I'm also attending the PhD program in Cybersecurity, another time at La Sapienza.

I mainly study Cybersecurity, especially binary exploitation, fuzzing,
microarchitectural attacks and hardware hacking.

I'm a proud member of [CeSeNA Security](https://cesena.github.io/) and 
[Hackappatoi](https://hackappatoi.github.io/).

I'm in love with the penguin 🐧.

I speak italian and english, I'm studying japanese and will study
french and spanish.

## Social

- [GitLab](https://gitlab.com/myasnik) 🥼
- [Linkedin](https://www.linkedin.com/in/pietro-mazzini-b3b8a4189/) 👥
- [Twitter](https://twitter.com/mazzini_pietro) 🐦 
