+++
title = "Research proposal: A Smart Approach to Infection Analysis and Infected ELF Binaries Disinfection"
date = "2022-05-22"
author = "Myasnik"
description = "This is a research proposal developed during my master degree in Sapienza."
tags = ["binary", "ELF",  "malware", "virus", "machine learning", "research proposal"]
+++

Hello everybody (me, myself, my cat and my mother), long time I don't write here.

This is a research proposal I developed about an year ago, during my Master degree in Sapienza university of Rome for the course Data and Network Security held by professor Mancini.

### Abstract

> The aim of this research proposal is to study and theorize a machine learning approach to ELF binaries structure anomaly detection. The neural network proposed should detect crafted or infected executable files and visually underline anomalies found to help researchers understand viruses behaviors and newly exploited binary crafting techniques. The second part of the research aim is restoring infected ELF binaries to their initial, harmless, stage.

### Disclaimers

- Nothing about this proposal has been implemented, the aim of the course was just to let us familiarize with proposal writing; however I think that the approach described in the attached PDF (even if not deeply explained) could be useful and stimulating for binary+machine learning new recruits.
- I'm not a machine learning expert - well, in reality I know nothing about it - so please take everything written in this proposal with a grain of salt.

### Resources and notes

After the submission of my work for the Data and Network Security course I started contacting professors for my Master thesis; with one of them i shared my proposal and he suggested me reading [this paper](https://www.eurecom.fr/en/publication/6603) which is actually quite connected with my idea, maybe you could find this interesting too!

### Research proposal

{{< embed-pdf url="/pdf/DNS_research_proposal___A_Smart_Approach_to_Infection_Analysis_and_Infected_ELF_Binaries_Disinfection.pdf" >}}

Here's the [overleaf](https://www.overleaf.com/read/vcbwdnktspdc) view-only project and the [pdf file](https://myasnik.gitlab.io/pdf/DNS_research_proposal___A_Smart_Approach_to_Infection_Analysis_and_Infected_ELF_Binaries_Disinfection.pdf).

Thanks for the attention, see ya.
