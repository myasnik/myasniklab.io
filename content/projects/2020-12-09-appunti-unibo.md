+++
title = "Appunti UNIBO - Ingegneria e Scienze Informatiche [IT]"
date = "2020-12-09"
author = "Myasnik"
description = "Projects, summaries and information about my bachelor degree."
tags = ["projects", "summaries", "bachelor", "UNIBO"]
+++

This project is just a collection of projects, summaries and information
about the bachelor I attended in Cesena: "Engineering and Computer Science".

The entire project (or the great majority) is in italian.

Subjects and themes (list not complete):
- Database
- High Performance Computing (HPC)
- Software Engineering (Ingegneria_sw)
- Networks (Reti)
- Networks 2 (Reti_2)
- Web Technologies (Tec_web)
- Internship (Tirocinio)
- Internet Of Things (IOT)
- Operational Research (Ricerca_operativa)
- Electronics (Elettronica)
- Systems Integrations (Systems_int)
- Numerical Methods (Metodi_numerici)
- Object Oriented Programming, Project (Programmazione_oggetti, progetto)

Here's the [repo](https://gitlab.com/myasnik/appunti-unibo-isi).
