+++
title = "OSSIHR - Open Source System for Incident Handling and Response"
katex = true
date = "2020-11-17"
author = "Myasnik"
description = "My bachelor thesis."
tags = ["projects", "ossihr", "bachelor", "UNIBO", "open source", "incident handling", "incident response"]
+++

# OSSIHR: Open Source System for Incident Handling and Response

The growing request for a monitoring and protection system applicable to all business environments spurred developers to work together to obtain a robust solution for those needs. Cyberloop (the agency where i worked for my thesis project) decided to contribute to this end by assigning to some of its trainees (me) the duty of implementing a system based on open-source technologies to protect agencies from cyber threats. The initial architectural idea was structured by Alessandro Molari, CEO and founder of the company as well as cybersecurity expert, and was subsequently reworked by the implementers. The main aim of this study is to initiate a community-driven project that maybe, one day, could become a better alternative to closed-source and paid cyberdefense systems. The result of this work has been called **OSSIHR: Open Source System for Incident Handling and Response**.

Before looking in-depth into OSSIHR and its functionalities let's define some useful terms and concepts.

## Introduction

**Incident Handling** consists of an organized and systematized process used to address cyber attacks and security breaches. The goal of this security approach is to mitigate and possibly avoid the damage of a potential security issue within an IT structure or department; **Incident Response** is the last step of Incident Handling, the effective and practical repair of the incident. **Intrusion Detection** consists of monitoring the events occurring in a computer system or network and analyzing them for signs of possible incidents. This practice is usually achieved using a category of software called Intrusion Detection Systems (IDS); these are one of the main components of Incident Handling and Response (IHR) platforms.

### Why does our company need an Incident Handling and Response platform?

An IHR platform is crucial for a company because it helps security analysts to defend the firm's infrastructure against cybersecurity attacks. Any security incident can escalate into something bigger if not handled quickly and properly. Without an appropriate incident handling service, these technical issues can result in a problematic security breach and, consequently, in a system collapse. Data loss, exploitation of vulnerabilities, service outages and future security breaches can be prevented through this platform. Thanks to IHR services an incident will never find you unprepared since you'll have already outlined how to manage and assess problems, so you'll be able to solve them with the right approach. In the same way, the remediation steps will have been already prepared so the financial and data loss will be curbed. These points will lead to a general improvement of the service offered and, supposedly, to an improvement in customer satisfaction.

### What is OSSIHR?

**OSSIHR** is a set of various free and open-source software that integrate with each other to provide useful functions for Incident Handling and Response processes.

Some of the key points about OSSIHR are:

- Detection of security threats in machines and networks through:
    - custom or pre-made sets of rules maintained by the community
    - log analysis
    - file integrity monitoring
    - CVE (Common Vulnerabilities and Exposure) databases
- Response to security threats in a semi-automatic and programmable way
- Quick and simple analysis of artifacts (files and information left by the attackers)
- Detection and prevention of data leaks
- Security intelligence: sharing of information with other companies facing similar issues

Everything is coordinated through a dynamic and customizable dashboard; this is fully integrated with each component of the system and an artificial intelligence engine working to help cybersecurity analysts to organize the entire process of Incident Handling. The distinctive characteristic of OSSIHR is that it is fully open-source; this means that this system can be easily extended and tailored to user requirements. Additionally, open-source projects bring other benefits because the extension of the software can be manually done or delegated to someone else, issues and bugs can be reported to developers by anyone, the same issues and bugs can be fixed by anyone.

## Architecture

### Why is OSSIHR's architecture interesting?

As the below diagram points out, the architecture is divided into two main locations: constituency (customer) and CSIRT (Computer Security Incident Response Team, in our case, Cyberloop). Constituencies could be sundry while the CSIRT must be unique; this means that OSSIHR isn't just an IHR platform but it also lends itself to an higher level of management because it has been conceived to be used by cybersecurity agencies to manage **various** customer's environments **with a single platform**.

![](/img/ossihr.png)

### How it works

Let's start by explaining how the system works by talking about **Wazuh**, one of the core components of OSSIHR. Wazuh is an Host Intrusion Detection System (HIDS) for threat detection, integrity monitoring, incident response and compliance. \\(\textcolor{red}{Wazuh\ Agents}\\) are monitoring software components that are installed on the company machines; these send logged events to the \\(\textcolor{blue}{Wazuh\ Manager}\\). This component analyzes the events provided in input and saves the results of these security analyses to \\(\textcolor{green}{ElasticSearch}\\), the heart and "database" of the entire system. Through **Kibana**, a modular web dashboard, information saved in ElasticSearch can be graphically visualized by cybersecurity analysts for further studies. Events analyzed by the Wazuh Manager are sent to **wazuh2thehive**, a simple event parser and translator that filters events, passing only the most dangerous ones to \\(\textcolor{orange}{TheHive}\\). This software is the control panel of the entire Incident Handling and Response system; through TheHive, cybersecurity analysts can track, handle and quickly analyze events (that now have become alerts). Alerts could contain useful information (IPs, email addresses, URLs, domains, files, hashes...) which we will hereafter refer to as observables; additionally, TheHive allows to merge alerts and organize them into cases. We chose TheHive to fulfill this function as it is easy to use, though it's not perfect: some of the main downsides being that cases can't be split into alerts, and observables can't be modified via web UI. The next piece of the puzzle is **OwlH**, a Network Intrusion Detection System (NIDS). \\(\textcolor{yellow}{OwlH\ Nodes}\\) are installed in every site of the company and analyze network traffic using **Suricata** and **Zeek** (network threat detection and logging engines) and send the result of the analysis to the Wazuh Manager, triggering the previously explained procedure. The captured traffic is forwarded by the OwlH Nodes to the \\(\textcolor{purple}{OwlH\ Master}\\) which is basically a web interface used to control all OwlH Nodes. Here, the traffic is shared with **Moloch**, a large scale indexed packet capture and search tool that graphs network traffic provided in input. Through this service cybersecurity analysts can manually scan network traffic for further investigations. OwlH is still in the development phase so there are some aspects that need improvement. The web UI is a bit unstable and some triggers do not work but its active and supporting community, the availability of developers in helping users and the good video and written documentation are incredibly useful resources that indicate a high level of dedication.

Together with the previously explained fundamental and structural components, we decided to add to OSSIHR some other functions that further simplify cybersecurity analysts' work; these are achieved through Cortex, MISP and AIL-Framework. \\(\textcolor{orange}{Cortex}\\) is an observable analysis and active response engine. TheHive uses Cortex's analyzers to enrich cases with information about connected observables. From TheHive's dashboard responders can also be triggered to quickly respond to alerts. To do so Cortex controls Wazuh Agents to activate procedures to contain attacks, executing real-time operation on assaulted machines. Cortex is also strictly integrated with \\(\textcolor{orange}{MISP}\\), a threat intelligence platform for sharing, storing and correlating Indicators Of Compromise (IOCs). These two software programs collaborate in information collection about observables. The usage of appropriate analyzers let Cortex use MISP to search information about known observables. MISP, instead, analyzes unknown observables using Cortex analyzer modules. Events collected by MISP can also be exported to TheHive and vice-versa, this leads to a continuous information sharing between different companies and entities. Cortex has been chosen for its powerfulness and extensibility, while MISP has been chosen for its stability and widespread use. In Cortex, the analyzers and responders cited above consists of Python scripts that can be easily created and modified, implementing custom checks and responses; the only downside of these scripts is that they aren't very stable and lack of a proper documentation. Talking about MISP it can be said that it's the most famous threat intelligence platform; its wide use means more users, which in turn means more sources from which obtain and share information, which are the core features of this kind of platform. Using MISP isn't trivial: there are a many functionalities and in general the UI is not intuitive, but with a bit of practice this problem can be easily solved. The last element of OSSIHR is \\(\textcolor{cyan}{AIL-Framework}\\), a modular framework used to analyze potential information leaks from unstructured data sources like pastes from Pastebin or similar services and, in general, from websites over the Clear Net and the Deep Web. It publishes alerts about detected data leaks on TheHive's dashboard and it can also share those alerts with the MISP community. This tool has a lot of functions - which is a plus - but they aren't very well organized, so everything appears a bit disordered.

In addition to these components, Cyberloop proposed another component to integrate into the system: the **Alert Aggregator**. This component analyzes TheHive's alerts using machine learning algorithms to automatically aggregate alerts into appropriate cases. It hasn't been developed yet but we plan to do this in the near future.

## Evaluation, comparison and conclusions

The table below draws a comparison between OSSIHR and other IHR systems functions. These platforms are developed by five cybersecurity colossus and represent the state of the art in closed-source cybersecurity systems (for an explanations of the functions see [Glossary](#glossary)).

| System \\(\rightarrow\\) <br> Function \\(\downarrow\\) |   OSSIHR  |  Rapid7  |   Cynet  | RSA SecOps | Symantec |   Cisco  |
|:-----------------------------------------------:|:---------:|:--------:|:--------:|:----------:|:--------:|:--------:|
| FIM                                             |  \\(\times\\) | \\(\times\\) | \\(\times\\) |            | \\(\times\\) | \\(\times\\) |
| Inventory management                            |  \\(\times\\) | \\(\times\\) | \\(\times\\) |  \\(\times\\)  | \\(\times\\) | \\(\times\\) |
| Log collection and retention                    |  \\(\times\\) | \\(\times\\) | \\(\times\\) |  \\(\times\\)  | \\(\times\\) | \\(\times\\) |
| Vulnerability assessment                        |  \\(\times\\) | \\(\times\\) | \\(\times\\) |  \\(\times\\)  |          |          |
| Antivirus                                       | \\(\approx\\) | \\(\times\\) | \\(\times\\) |  \\(\times\\)  | \\(\times\\) | \\(\times\\) |
| Network analytics and response                  |  \\(\times\\) | \\(\times\\) | \\(\times\\) |  \\(\times\\)  | \\(\times\\) | \\(\times\\) |
| Endpoint detection and response                 |  \\(\times\\) | \\(\times\\) | \\(\times\\) |  \\(\times\\)  | \\(\times\\) | \\(\times\\) |
| Deception                                       |           | \\(\times\\) | \\(\times\\) |            | \\(\times\\) | \\(\times\\) |
| User behavior analytics                         |           | \\(\times\\) | \\(\times\\) |  \\(\times\\)  | \\(\times\\) | \\(\times\\) |
| Pre-set remediation                             |           | \\(\times\\) | \\(\times\\) |  \\(\times\\)  |          | \\(\times\\) |
| Custom remediation                              |           | \\(\times\\) | \\(\times\\) |  \\(\times\\)  |          |          |
| Automated playbooks                             |           | \\(\times\\) | \\(\times\\) |  \\(\times\\)  |          |          |
| Attacker behaviour analysis                     |           | \\(\times\\) |          |            | \\(\times\\) | \\(\times\\) |
| Dynamic and live dashboards                     |  \\(\times\\) | \\(\times\\) | \\(\times\\) |  \\(\times\\)  | \\(\times\\) | \\(\times\\) |
| Leak detection (clearnet)                       |  \\(\times\\) | \\(\times\\) |          |            |          |          |
| Leak detection (deep web)                       |  \\(\times\\) |          |          |            |          |          |
| Policy assessment                               |  \\(\times\\) | \\(\times\\) |          |  \\(\times\\)  | \\(\times\\) |          |
| Threat intelligence                             |  \\(\times\\) | \\(\times\\) |          |  \\(\times\\)  | \\(\times\\) | \\(\times\\) |
| Email protection                                |           |          | \\(\times\\) |            | \\(\times\\) | \\(\times\\) |

As you can see, OSSIHR has fewer functions than other systems. So it may seem that it can't compete with its rivals, but we disagree for two main reasons. First of all, OSSIHR is open-source, so these functions can be implemented at a later date or be implemented directly by the user. Secondly, OSSIHR isn't just an IHR service but, as already mentioned in the architecture section, it is also an entire platform that can and should be used by security agencies to handle the security of multiple customer-companies simultaneously through the same system. This means that, for example, Cyberloop could use just one instance of OSSIHR to manage all of its clients.

### Conclusions

It can be said that, currently, OSSIHR isn't a robust and competitive alternative to closed-source IHR systems. This is due to the lack of some important functions and the presence of mechanisms not yet mature. OSSIHR clearly has aspects that need improving: the UIs in general are disorganized and unstable, the wazuh2thehive is still immature and the documentation of some of the components is incomplete or not present. However, being this project open-source means that the margins of improvement are vast and extension and personalization possibilities are manifold. Extending most of the components is trivial since the components are meant to be extended and provide models that could be used to develop new features. Sometimes, manually extending components could even be unnecessary, because the community may have already implemented what we need. The higher level of abstraction that OSSIHR gives to the user is also an interesting macro-function that distinguishes our platform from its competitors.

## Glossary

- **File Integrity Monitoring (FIM)**: detect important file changes using checksum.
- **Inventory management** : real-time surveillance of installed software and executing processes on monitored endpoints.
- **Log collection and retention** : storing of network and system logs.
- **Vulnerability assessment** : collection of information about installed software. Data obtained are then compared with Common Vulnerabilities and Exposures (CVE) databases to identify vulnerable software.
- **Network analytics and response** : intrusion detection and incident response, network side.
- **Endpoint detection and response** : intrusion detection and incident response, hosts side.
- **Deception** : creation and diffusion of false passwords, files and hosts to confuse attackers.
- **User behavior analytics** : profiling and monitoring of user's activities to detect anomalous actions using a behavioral study.
- **Attacker behavior analytics** : profiling and monitoring of attacker's activities to block in advance malicious operations.
- **Pre-set remediation** : set of default tools that automatically respond to well-known alerts.
- **Custom remediation** : same as pre-set remediation but customizable.
- **Automated playbooks** : ordered sequences of pre-set and custom remediation steps used to resolve an incident.
- **Leak detection** : detection and report of data leaks found on the Clear and Deep Web.
- **Policy assessment** : system and applications configurations monitoring to verify that they are consistent with defined security policies.
- **Threat intelligence** : information exchange about systems threats like malware and data leaks.
- **Email protection** : protection against malicious email attachments.

## References

- [Wazuh](https://wazuh.com/): Host Intrusion Detection System (HIDS) for threat detection, integrity monitoring, incident response and compliance.
- [OwlH](https://www.owlh.net/): Network Intrusion Detection System (NIDS); visualize and manage Suricata, Zeek &amp; Moloch.
  - [Suricata](https://suricata-ids.org/): mature, fast and robust network threat detection engine capable of real time intrusion detection, inline intrusion prevention, network security monitoring and offline PCAP processing.
  - [Zeek](https://zeek.org/): hardware, software, virtual, or cloud platform that quietly and unobtrusively observes network traffic; it interprets what it sees and creates compact, high-fidelity transaction logs, file content, and fully customized output, suitable for manual review on disk or in a more analyst-friendly tool like a Security and Information Event Management (SIEM) system.
  - [Moloch](https://molo.ch/): large scale indexed packet capture and search that graphs network traffic captured by OwlH.
- [Elastic Stack](https://www.elastic.co/what-is/elk-stack)
  - [Elasticsearch](https://www.elastic.co/): search and analytics engine based on JSON format, distributed, scalable and RESTful.
  - [Kibana](https://www.elastic.co/kibana): lets users visualize data with charts and graphs in Elasticsearch.
  - [Beats](https://www.elastic.co/beats/): send data from hundreds or thousands of machines and systems to Elasticsearch.
- [AIL-Framework](https://github.com/CIRCL/AIL-framework): modular framework to analyze potential information leaks from unstructured data sources like pastes from Pastebin or similar services and websites over the Clear Net and the Deep Web.
- [TheHive](https://thehive-project.org/): scalable security incident response platform, the web dashboard over the entire incident handling system.
- [Cortex](https://thehive-project.org/): observable (IPs, email addresses, URLs, domains, files, hashes...) analysis and active response engine.
- [MISP](https://www.misp-project.org/): threat intelligence platform for sharing, storing and correlating indicators of compromise of targeted attacks, threat intelligence, financial fraud information, vulnerability information or even counter-terrorism information.
- [Thesis repo](https://gitlab.com/myasnik/tesi-ossihr) (IT)
- [Presentation repo](https://gitlab.com/myasnik/slide-ossihr) (IT)
