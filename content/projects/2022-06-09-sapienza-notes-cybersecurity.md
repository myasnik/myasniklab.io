+++
title = "Sapienza Notes - Cybersecurity"
date = "2022-06-09"
author = "Myasnik"
description = "Notes wrote by me during the period of my master's degree at Sapienza unviersity of Rome (Course: Cybersecurity)"
tags = ["Sapienza", "cybersecurity",  "notes", "master"]
+++

Hello everybody, this is my master's degree notes repository.

This could be useful to students of my master degree to get feedback about courses. Another use case is understanding the course focus and topics for people interested in attending this master course; maybe if you're undecided in what master degree course to attend here you could find answers to your questions.

Here's the [repo](https://gitlab.com/myasnik/sapienza-notes-cybersecurity).

Thanks for the attention, hope that it will be useful!

See ya.
