+++
title = "Led Hearts"
date = "2020-12-10"
author = "Myasnik"
description = "Turn on a LED a thousand kilometers away to tell your love you're thinking about her."
tags = ["valentine's day", "girlfriend",  "raspberry", "esp8266", "MQTT", "AdafruitIO"]
+++

> "Turn on a LED a thousand kilometers away to tell your love you're
> thinking about her."

This is a very simple yet effective project!

Using MQTT protocol you can create remote lights
that can be controlled via smartphones.

Supported hardware:
- Raspberry Pi (tested: model 2B)
- ESP8266
- Orange PI (tested: model one)

I thought about this when my girlfriend had to come back to her home town,
she had a similar idea and I applied her idea to FOSS and cheap hardware.

Here's the [repo](https://gitlab.com/myasnik/led-hearts)
